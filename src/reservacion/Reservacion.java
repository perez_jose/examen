/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservacion;

import java.util.ArrayList;

/**
 *
 * @author CompuStore
 */
public class Reservacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Dispositivo GalaxyS10 = new Dispositivo();
        GalaxyS10.setMarca("Samsung");
        GalaxyS10.setModelo("S10");
        GalaxyS10.setTipo("Smartphone");
        GalaxyS10.setPrecio(700);
        
        Dispositivo GalaxyS9 = new Dispositivo();
        GalaxyS9.setMarca("Samsung");
        GalaxyS9.setModelo("S9");
        GalaxyS9.setTipo("Smartphone");
        GalaxyS9.setPrecio(600);
        
        
         Dispositivo LG = new Dispositivo();
        LG.setMarca("LG");
        LG.setModelo("R300");
        LG.setTipo("SmarTV");
        LG.setPrecio(400);
         
        Reserva PrimeraReserva = new Reserva(10, GalaxyS10);
        Reserva SegundaReserva = new Reserva(2, GalaxyS9);
        Reserva TerceraReserva = new Reserva(5, LG);
           
        
        Cliente Jahir = new Cliente();
        Jahir.setApellidos("Suarez");
        Jahir.setNombres("Fernando Jahir");
        Jahir.setCedula("1112223334");
        Jahir.setEmail("Jahirproooo@hotmail.ec");
       
        
        
        Jahir.ListaReservas = new ArrayList<>();
        Jahir.ListaReservas.add(PrimeraReserva);
        Jahir.ListaReservas.add(SegundaReserva);
        Jahir.ListaReservas.add(TerceraReserva);
        
        Pedido miPedido = new Pedido(Jahir);
        ImprimirPedido imprimir = new ImprimirPedido();
       imprimir.Imprimir(Jahir);
        // cliente 2
          Dispositivo audifonos = new Dispositivo();
        audifonos.setMarca("Samsung");
        audifonos.setModelo("I7");
        audifonos.setTipo("Smartphone");
        audifonos.setPrecio(10);
        
        Dispositivo GalaxyS8 = new Dispositivo();
        GalaxyS8.setMarca("Samsung");
        GalaxyS8.setModelo("GalaxyS8");
        GalaxyS8.setTipo("Smartphone");
        GalaxyS8.setPrecio(600);
        
        
         Dispositivo riviera = new Dispositivo();
        riviera.setMarca("LG");
        riviera.setModelo("R300");
        riviera.setTipo("SmarTV");
        riviera.setPrecio(400);
         
        Reserva Reserva1 = new Reserva(10, audifonos);
        Reserva Reserva2 = new Reserva(2, GalaxyS8);
        Reserva Reserva3 = new Reserva(5, riviera);
           
        
        Cliente Kerly = new Cliente();
        Kerly.setApellidos("Suarez");
        Kerly.setNombres("Kerly Anahi");
        Kerly.setCedula("1112223334");
        Kerly.setEmail("kerly@hotmail.ec");
        
        
        Kerly.ListaReservas = new ArrayList<>();
        Kerly.ListaReservas.add(Reserva1);
        Kerly.ListaReservas.add(Reserva2);
        Kerly.ListaReservas.add(Reserva3);
        
        Pedido pedido = new Pedido(Kerly);
        ImprimirPedido imprimir1 = new ImprimirPedido();
       imprimir1.Imprimir(Kerly);
        //cliente 3
         Dispositivo Tablet = new Dispositivo();
        Tablet.setMarca("Samsung");
        Tablet.setModelo("Tablet");
        Tablet.setTipo("Smartphone");
        Tablet.setPrecio(200);
        
        Dispositivo Laptop = new Dispositivo();
        Laptop.setMarca("Dell");
        Laptop.setModelo("I core 5");
        Laptop.setTipo("portatil");
        Laptop.setPrecio(700);
        
        
         Dispositivo tv = new Dispositivo();
        tv.setMarca("riviera");
        tv.setModelo("R300");
        tv.setTipo("SmarTV");
        tv.setPrecio(400);
         
        Reserva ReservaN = new Reserva(4, Tablet);
        Reserva ReservaM = new Reserva(2,Laptop);
        Reserva ReservaO = new Reserva(5, tv);
           
        
        Cliente Sandy = new Cliente();
        Sandy.setApellidos("Cedeño");
        Sandy.setNombres("Sandy Johanna");
        Sandy.setCedula("1112223334");
        Sandy.setEmail("Sandy@hotmail.ec");
        
        
        Sandy.ListaReservas = new ArrayList<>();
        Sandy.ListaReservas.add(ReservaN);
        Sandy.ListaReservas.add(ReservaM);
        Sandy.ListaReservas.add(ReservaO);
        
        Pedido Namber = new Pedido(Sandy);
        ImprimirPedido imprimirO = new ImprimirPedido();
       imprimirO.Imprimir(Sandy);
        // cliente 4
         Dispositivo J2Prim = new Dispositivo();
        J2Prim.setMarca("Samsung");
        J2Prim.setModelo("J2Prim");
        J2Prim.setTipo("Smartphone");
        J2Prim.setPrecio(120);
        
        Dispositivo P30 = new Dispositivo();
        P30.setMarca("Huwey");
        P30.setModelo("P30");
        P30.setTipo("Smartphone");
        P30.setPrecio(350);
        
        
         Dispositivo Computador = new Dispositivo();
        Computador.setMarca("Dell");
        Computador.setModelo("221321");
        Computador.setTipo("Escritorio");
        Computador.setPrecio(400);
         
        Reserva ReservaA = new Reserva(10, J2Prim);
        Reserva ReservaB = new Reserva(2, P30);
        Reserva ReservaC = new Reserva(5, Computador);
           
        
        Cliente Diana = new Cliente();
        Diana.setApellidos("Perez");
        Diana.setNombres("Diana Edilma");
        Diana.setCedula("1112223334");
        Diana.setEmail("Diana@hotmail.ec");
        
        
        Diana.ListaReservas = new ArrayList<>();
        Diana.ListaReservas.add(ReservaA);
        Diana.ListaReservas.add(ReservaB);
        Diana.ListaReservas.add(ReservaC);
        
        Pedido pedidos = new Pedido(Diana);
        ImprimirPedido imprimirr = new ImprimirPedido();
       imprimirr.Imprimir(Diana);
        // cliente 5
         Dispositivo Alcatel = new Dispositivo();
        Alcatel.setMarca("Alcatel");
        Alcatel.setModelo("Alcatel");
        Alcatel.setTipo("Smartphone");
        Alcatel.setPrecio(240);
        
        Dispositivo P20 = new Dispositivo();
        P20.setMarca("Hawey");
        P20.setModelo("P20");
        P20.setTipo("Smartphone");
        P20.setPrecio(300);
        
        
         Dispositivo Telvisor = new Dispositivo();
        Telvisor.setMarca("LG");
        Telvisor.setModelo("R300");
        Telvisor.setTipo("SmarTV");
        Telvisor.setPrecio(400);
         
        Reserva ReservaD = new Reserva(10, Alcatel);
        Reserva ReservaF = new Reserva(2, P20);
        Reserva ReservaG = new Reserva(5, Telvisor);
           
        
        Cliente Jose = new Cliente();
        Jose.setApellidos("Perez");
        Jose.setNombres("Jose Luis");
        Jose.setCedula("1112223334");
        Jose.setEmail("Jose@hotmail.ec");
        
        
        Jose.ListaReservas = new ArrayList<>();
        Jose.ListaReservas.add(ReservaD);
        Jose.ListaReservas.add(ReservaF);
        Jose.ListaReservas.add(ReservaG);
        
        Pedido order = new Pedido(Jose);
        ImprimirPedido print = new ImprimirPedido();
       print.Imprimir(Jose);
        
    }
    
}
